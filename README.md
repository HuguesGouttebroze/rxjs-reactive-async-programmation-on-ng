# Rxjs Tuto HuguG
## App ARCHITECTURE
- Components
  - CRUD user 
  - auth-view
  - error view
  - header
  - TODO: layout with header + navbar & footer
- Services (équvalent des "STATES" || "états")
  - voir pr imp avec NgRX
- Modèles
- Views
## Observer & Subscriber
* Observer: surveil. la production des elements
        +
    équivalent RxJS :

    - prochaine boite sur la bande
        ===
        next()
        (params === item)

    - si une erreur?
    trouver 1 solution tt de suite (résolution de l'erreur)
        ===
        error()
        (params === err)

    - Fin journée => stopper la production / les machines
        ===
        complete()
    (soit la fin des valeurs de souscription, 
    les event sont finis, 
    cette méthode ne prend aucun parametre)

### SUBSCRIBER
* a les memes methodes mais avec 1 en + !

    => `onSubscribe()` permettant de se détacher d'une chaîne d'Obdervables à laquel nous avons souscris

### OBSERVABLE === 1 fnc de rappel

- soit 1 callback (bref 1 fnc passée en parametre d une autre fnc)

### Qu'es-ce qu'une Séquence d'Observables ?

- pt etre un ensemble d observables de != types (Strings, Numbers, Events (keydown,keyup, ...) Object, Réponse HTTP Requête, autres observables ...)

- l observable pt etre soit syncrone soit asyncrone

#### Comment démarrer notre séquence ?

- avec la méthode `subscribe` pr lancer la séquence
- on appelle la séquence, puis la mét. `subscribe` en lui passant le nom de l'Observer (ou observateur, enfin un objet surveillé par l'Observable ... )

    ---> tel que : 
```ts
stream.subscribe(observer$);
```
- Ceci permet de démarrer notre séquence d'observables.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.2.1.

### Training on RxJS & reactive programmation

#### with scraping

- Warning, the goal of this scraping exercise is only to learn RxJS, with some real features & issues programming.

```ts
const { BehaviorSubject } =  require('rxjs');

const  baseUrl  =  `https://imdb.com`;

const  allUrl$  =  new  BehaviorSubject(baseUrl);
```

## RxJS, Websockets & NodeJS

    - => Messages over sockets as Observables

    - => Sockets can be used to send messages from the client to the server and vice versa. With the socket.io library, we can send messages using the emit method.

    ```ts
    SocketIO.Socket.emit(event: string, …args: any[]): SocketIO.Socket
    ```

    - => The parameter event can be seen as an identifier of the type of message we want to send. The …args parameters can be used to send data specific to a single message.
    - => 

    - => Whoever is interested in a certain type of message (or event, to use the socket.io terminology) can start listening on the socket using the method on.

    ```ts
    SocketIO.Emitter.on(event: string, fn: Function): SocketIO.Emitter
    ```
    
## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
