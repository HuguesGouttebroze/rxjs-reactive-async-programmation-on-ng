import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddUserViewComponent } from './add-user-view/add-user-view.component';
import { AuthGuardService } from './auth/auth-guard/auth-guard.service';
import { AuthViewComponent } from './views/auth-view/auth-view.component';
import { EditUserViewComponent } from './views/edit-user-view/edit-user-view.component';
import { ErrorViewComponent } from './views/error-view/error-view.component';
import { SingleUserViewComponent } from './views/single-user-view/single-user-view.component';

const routes: Routes = [
  { path: 'user/edit/:id', canActivate: [AuthGuardService], component: EditUserViewComponent},
  { path: 'auth', component: AuthViewComponent }, // authenticate road
  { path: 'users', canActivate: [AuthGuardService], component: SingleUserViewComponent},
  { path: 'user/new', canActivate: [AuthGuardService], component: AddUserViewComponent},
  { path: 'user/:id', canActivate: [AuthGuardService], component: SingleUserViewComponent},
  // { path: 'profil', canActivate: [AuthGuardService], component: ProfilViewComponent},
  { path: '', canActivate: [AuthGuardService], component: SingleUserViewComponent },
  { path: 'not-found', component: ErrorViewComponent },
  { path: '**', redirectTo: 'not-found' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
