import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { SingleUserViewComponent } from './views/single-user-view/single-user-view.component';
import { EditUserViewComponent } from './views/edit-user-view/edit-user-view.component';
import { AuthViewComponent } from './views/auth-view/auth-view.component';
import { ErrorViewComponent } from './views/error-view/error-view.component';
import { HeaderComponent } from './components/header/header.component';
import { AuthService } from './services/auth/auth.service';
import { UserService } from './services/user/user.service';
import { UtilService } from './util/util.service';
import { AddUserViewComponent } from './add-user-view/add-user-view.component';
// import { BooksService } from './services/books/books.service';
// import { BookComponent } from './book/book.component';
// import { BooksViewComponent } from './views/books-view/books-view.component';
// import { AuthService } from './services/auth/auth.service';
// import { ErrorViewComponent } from './error-view/error-view.component';
// import { SingleBookViewComponent } from './views/single-book-view/single-book-view.component';
// import { HeaderComponent } from './header/header.component';
// import { AuthViewComponent } from './views/auth-view/auth-view.component';
// import { NewBookViewComponent } from './views/new-book-view/new-book-view.component';
// import { EditBookViewComponent } from './edit-book-view/edit-book-view.component';
// import { ProfilViewComponent } from './views/profil-view/profil-view.component';
// import {UserService} from './services/user/user.service';

@NgModule({
  declarations: [
    AppComponent,
    SingleUserViewComponent,
    EditUserViewComponent,
    AuthViewComponent,
    ErrorViewComponent,
    HeaderComponent,
    AddUserViewComponent,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    UtilService,
    AuthService,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
