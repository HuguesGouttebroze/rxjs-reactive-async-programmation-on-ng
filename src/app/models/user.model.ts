 export class User {
    /* private _id: number;
    private _email: string;
    private _first_name: string;
    private _last_name: string;
    private _avatar: string; */
constructor(
    public id: number,
    public first_name: string,
    public last_name: string,
    public email: string,
    public avatar: string
  ) {}
 }
