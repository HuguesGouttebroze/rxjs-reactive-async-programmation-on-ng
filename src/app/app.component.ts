import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, BehaviorSubject, from, interval, Subscription } from 'rxjs';
import { map, distinct, filter, mergeMap } from 'rxjs/operators';
import { User } from './models/user.model';
import { UserService } from './services/user/user.service';
// import { rp } from 'request-promise-native';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  title = 'rxjs-tuto-hug';
  users: Array<User> = [];
  /**
   * from HttpClient request
   */
  usersSubscription: Subscription = new Subscription;
  constructor(private userService: UserService) {}
  // private subscription: Subscription[] = [];//            OR create a "new Subscription()"//            with the ".add" method on start()
  private subscription: Subscription = new Subscription();

  ngOnInit() {
    /**
     * HttpClient PART
     */
    this.usersSubscription = this.userService.getUsers().subscribe(
      (users: Array<User>) => this.users = users // dispath datas users into user array
    );
    /**
     * Observable RxJS PART
     */
    const observer$ = {
      next: (item: unknown) => console.log(`Une boîte arrive ${item}.`),
      error: (err: unknown) => console.log(`Oupsss! Erreur: ${err}.`),
      complete: () => console.log(`Fin de notre séquence d'observables.`)
    };

    const stream$ = new Observable(myObserver => {
      myObserver.next("BOÎTE n°1");
      myObserver.next("BOÎTE n°2");

      /* for (let index = 0; index < 10000; index++) {
        myObserver.next(`Boîte n° ${index}`);
      } */
      myObserver.next("BOÎTE n°3");

      myObserver.complete();
    });

    // using the `subscribe` method to start our SEQUENCE
    //stream$.subscribe(observer$);
    stream$.subscribe(
      /* item => console.log(`Une boîte arrive ${item}.`),
      err => console.log(`Oups, ${err}.`), */
      () => console.log(`Fin de notre séquence d'observables.`)
    );

    /* stream$.unsubscribe(); */

    // Scra ping movies datas
    const  baseUrl  =  `https://imdb.com`;

    const  allUrl$  =  new  BehaviorSubject(baseUrl);

    // const  normalizeUrl  =  require('normalize-url');

    const  uniqueUrl$  =  allUrl$.pipe(    // only crawl IMDB url
    filter(url  =>  url.includes(baseUrl)),    // normalize url for comparison
    // map(url  =>  normalizeUrl(url, { removeQueryParameters: ['ref', 'ref_']     })),
    // distinct is a RxJS operator that filters out duplicated values
    distinct());
  }

  ngOnDestroy() {
    this.usersSubscription.unsubscribe();
  }
  public start():void {
    //this.subscription.push(interval(30).subscribe(
      // change ".push" to ".add"
    this.subscription.add(interval(30).subscribe(
      // value => document.write(`|-o-o|-oo-|o-o-|--o${value}                                                                                    `), // next ! decrapeted
      value => console.log(`VALUE => ${value}`),
      //(`|-o-o|-oo-|o-o-|--o${value}                                                                                    `), // next ! decrapeted
      // error => document.write(error),                         // error ! decrapeted
      error => console.log(error),
      // error ! decrapeted
      () => console.log('sequence terminée')
      //() => document.write(`-o---o---o---|--`),               // complete ! ok, not decrapeted
    ))

    // add a second Observable Sequence to see the controle comportement
    this.subscription.add(interval(30).subscribe(
      value => console.log(`===+++ooo---=> ${value}`),
      error => console.log(error),
      () => console.log('sequence terminée')
    ))
  }
  public stop():void {
    // this.subscription.unsubscribe()
    /* this.subscription.forEach(element => {
      element.unsubscribe();
    }) */

    // & using ".add", we can use directly "unsubscribe()"
    // & RxJS will doing the itteration for us !!!
    this.subscription.unsubscribe();
  }
}
