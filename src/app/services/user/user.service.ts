import { Injectable } from '@angular/core';
import { BehaviorSubject, OperatorFunction } from 'rxjs';
import { User } from '../../models/user.model';
import { Observable } from 'rxjs';
import {map} from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  apiUrl = 'https://reqres.in';

  githubUrl = 'https://api.github.com/search/users';

  users: BehaviorSubject<Array<User>>;

  constructor(private httpClient: HttpClient) {
    this.users = new BehaviorSubject<Array<User>>([]);
  }

  /**
   * Methode to retrieve all API's users
   * @returns
   */
  getUsers() {

    return this.httpClient
      .get(this.apiUrl + '/api/users')///users/{username}/repos

      .pipe(
        map((res:any) => {
          const arrayUsers = res.data.map((item: { id: number; email: string; first_name: string; last_name: string; avatar: string; }) => {
            return new User(item.id, item.email, item.first_name, item.last_name, item.avatar)
          });
          this.users.next(arrayUsers);
          return this.users.getValue();
        })
      );
  }
}
/* function pipe(arg0: OperatorFunction<any, User[]>) {
  throw new Error('Function not implemented.');
}
 */
